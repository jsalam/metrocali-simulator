package controlPanel;

import controlP5.ControlP5;
import processing.core.*;

public class Panel {
	private static ControlP5 cp5;
	private int hght, wdth;
	public PApplet app;

	public Panel(PApplet app, int wdth, int hght) {
		cp5 = new ControlP5(app);
		// frame
		this.wdth = wdth;
		this.hght = hght;
		this.app = app;

		// add interface elements
		cp5.addSlider("tickSpeed").setPosition(20, 60).setRange(1000, 100)
				.setValue(800).setColorActive(0xffAFA14E)
				.setColorForeground(0xff8E8C14);
	}

	public void show(){
		app.fill(0,190);
		app.rectMode(PConstants.CORNER);
		app.rect(0,0,app.width,hght);
		app.rectMode(PConstants.CENTER);
	}

	public static ControlP5 getControlP5() {
		return cp5;
	}
}
