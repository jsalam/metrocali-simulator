import processing.core.*; 
import processing.opengl.*; 

import processing.net.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class Bus_Route_Simulator_Client_v1_4 extends PApplet {

/**
 * Adapted from
 * Shared Drawing Canvas (Client) 
 * by Alexander R. Galloway. 
 * 
 * @Version 1.0
 * @Date:  Jan 8 2014
 * @Author: Juan Salamanca
 */

Client theClient;
String input;

public void setup() 
{
  size(100, 100);
  // Connect to the server's IP address and port
  try{
  theClient = new Client(this, "127.0.0.1", 12345); // Replace with your server's IP and port
  }catch(Exception e){
	 println("Conection refused. Server might not be running"); 
  }
}

public void draw() 
{
  // Receive data from server
  if (theClient.available() > 0) {
    input = theClient.readString();
    input = input.substring(0, input.indexOf("\n")); // Only up to the newline
    println(input);
    //data = int(split(input, ' ')); // Split values into an array
    // Draw line using received coords

  }
  else{
	// println("DRAW / No client available"); 
  }
}

  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "--full-screen", "--bgcolor=#666666", "--stop-color=#cccccc", "Bus_Route_Simulator_Client_v1_4" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
