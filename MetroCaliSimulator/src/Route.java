import java.util.ArrayList;

import processing.core.PApplet;
import processing.core.PVector;


public class Route {
  private String id;
  private Station [] stations;
  private int[] stationIDs;
  // Buses
  ArrayList <Bus> fleet;
  int busCount;
  // Timing
  private int tickModule; // defines the pace of ticks in seconds
  private int controlClock = 0; // allows only one operation per tick eventhough the method is invoked many times per second.  
  private int tickcount = 0;
  
  public Station[] getStations() {
	return stations;
  }

PApplet app;


  /** 
   *  Creates an instance to the route. Buses at the first and the last station
   *  in the route don't drop or pick up passengers at those stations
   */
  Route(PApplet app, String id, int stopNum, int tickModule, int positionY) {
	  this.app = app;
    this.id = id;
    createStations(stopNum);
    setVerticalPosition(positionY);
    // Buses
    busCount = 0;
    fleet = new ArrayList <Bus>();
    this.tickModule = tickModule;
    fleet.add(newBus());
  }

  /** 
   *  Populates an array with stations.The first and the last stations are
   *  patios where the buses get ready to start their next route but they 
   *  don't drop or pick up passengers at those stations
   */
  private void createStations(int stopNum) {
    stations = new Station[stopNum];
    int origin = 50;
    // Starting patio
    stations [0] = new Station(app,0, 0, stopNum); // demand is set to 0
    int distStations = app.width/(stopNum+1);
    stations [0].setLocation(new PVector (origin, 0));
    stations [0].setTail();

    // In betweeen stations
    for (int i=1; i< stations.length -2; i++) {
      stations[i] = new Station(app, i, app.ceil(app.random(1, 7)), stopNum); // id, demand
      stations [i].setLocation(new PVector (origin + distStations*(i), 0));
    }
    // Last stop. Last passengers getting off
    stations [stations.length-2] = new Station(app,stations.length-2, 0, stopNum); // demand is set to 0
    stations [stations.length-2].setLocation(new PVector ( origin + distStations*(stations.length-2), 0));

    //Final Patio
    stations [stations.length-1] = new Station(app,stations.length-1, 0, stopNum); // demand is set to 0
    stations [stations.length-1].setLocation(new PVector ( origin + distStations*(stations.length-1), 0));
    stations [stations.length-1].setTail();

    // collects the station ids
    populateStationsID();
  }

  /** 
   *  Executes the route operations
   */
  public void runRoute(int systemClock) {
    // Fleet operation
    for (int i=0; i< fleet.size(); i++) {
      Bus temp = fleet.get(i);
      temp.run(controlPanel.Panel.getControlP5().getValue("tickSpeed"));
      // Stations operation
      for (int j=0; j< stations.length; j++) {
        stations[j].operate(temp);
      }
    }

    //Fleet generation
    if (systemClock % tickModule == 0 && systemClock != controlClock) {
      fleet.add(newBus());
      controlClock = systemClock;
      tickcount++;
    }

    // Messages on canvas
    PVector temp = stations[0].getLocation();
    app.fill(100, 250, 250);
    app.textAlign(app.LEFT, app.CENTER);
    app.text("Route "+ id, temp.x - 40, temp.y - 13);
    app.fill(220, 220, 150, 90);
    app.text("Fleet interval:" + tickModule + "min,    Ticks :"+ tickcount + ",   Total Buses: " + busCount, temp.x -40, temp.y - 27);
    app.textAlign(app.CENTER, app.CENTER);

    for (int j=0; j< stations.length; j++) {
      stations[j].show();
    }
    tickcount = stations[0].getTickcounter();
  }

  /** 
   *  populate the stationIDs collection with the IDs of all the stations in the route
   */
  private void populateStationsID() {
    stationIDs = new int [stations.length]; 
    // println("Buses on the route "+ id + " could stop at :");
    for (int i=0; i<stationIDs.length; i++) {
      stationIDs[i] = stations[i].getID();
      // print(id + ""+stationIDs[i]+", ");
    }
    app.println("");
  }

  /** 
   *  Gets the IDs of all the stations in the route
   */
  public int[] getStationsID() {
    return stationIDs;
  }

  /** 
   *  Gets the location of a station in a given route. The parameter indicates the station position in the route's list of stations 
   */
  public PVector getStationLocation (int stationListPosition) {
    return stations[stationListPosition].getLocation();
  }

  /** 
   *  Gets the station with the specified ID. The ID is the position in the array of stations 
   */
  public Station getStation (int id) {
    return stations[id];
  }

  /** 
   *  Sets the vertical position on stations on screen 
   */
  public void setVerticalPosition(int pos) {
    for (int i=0; i<stations.length; i++) {
      PVector temp = stations[i].getLocation();
      temp.y = pos; 
      stations[i].setLocation(temp);
    }
  }

  /** 
   *  Sets a station in the route 
   */
  public void setStation(int thePosition, Station oneStation) {
    stations[thePosition] = oneStation;
    // collects the station ids
    populateStationsID();
  }

  /** 
   *  Sets a station in the route 
   */
  public String getID() {
    return id;
  }

  // ********************** timing control ******************

  /** 
   *  Returns the bus generation pace of this route
   */
  public int getTickModule() {
    return tickModule;
  }
  /** 
   *  Changes the bus generation pace of this route
   */
  public void setTickModule(int module) {
    tickModule = module;
  }

  // ************************* methods for bus control *************************

  /** 
   *  Returns a new instance of the Bus class on the specified route
   */
  public Bus newBus() {
    /* {author=Juan Salamanca, version=1, since=Jan 2014}*/
    Bus bTemp = new Bus(app, (100)+busCount); //int id
    bTemp.setupRoute(this);
    busCount ++;
    return bTemp;
  }

  /** 
   *  Returns a new instance of the Bus class on the specified route that stops at the specified stop list
   */
  public Bus newBus(Route theRoute, int [] theStops) {
    /* {author=Juan Salamanca, version=1, since=Jan 2014}*/
    Bus bTemp = new Bus(app, (100)+busCount); //int id
    bTemp.setupRoute(this);
    bTemp.setupStops(theStops);
    busCount ++;
    return bTemp;
  }

  // ******************************* Reports ************************

  /** 
   *  returns a description of the route: ("Route "+id+" "+stations.length+"\n");
   */
  public String description() {
    String description = ("Route "+id+" "+stations.length+"\n");
    return description;
  }

  /** 
   *  returns a report of the route: ("Station "+id+" "+getCrowdSize()+" "+getOffPassenger()+" "+visitingBuses.size()+"\n");
   */
  public String report(boolean stat, boolean pass, boolean buses, boolean busesAtStation) {
    String report = ("Route "+id+" "+fleet.size()+"\n");
    if (stat) {
      report = report + "  Station (id inPassengers outPassengers numVisitingBuses isTail)\n";
      for (int i=0; i < stations.length ;i++) {
        Station temp = stations[i];
        report = report+"   "+temp.report(pass);
      }
    }
    if (buses) {
      report = report + "  Bus (id onBoardSize passCounter location.x location.y)\n";
      for (int i=0; i < fleet.size() ;i++) {
        Bus temp = fleet.get(i);
        report = report+"   "+temp.reportOnDemand();
      }
    }

    if (busesAtStation) {
      report = report + "  Bus (id remainingCapacity gotIn gotOut station)\n";
      for (int i=0; i < fleet.size() ;i++) {
        Bus temp = fleet.get(i);
        report = report+"   "+temp.reportAtStop;
      }
    }
    return report;
  }
}
