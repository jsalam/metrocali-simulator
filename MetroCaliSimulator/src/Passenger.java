import java.util.ArrayList;

/** 
 *  Represents an individual travelling from one station to another. 
 *  The id identifies the passenger. Destination and origin parameters 
 *  are the stations' id where the passenger got in and out of the 
 *  transportation system
 */

public class Passenger {
  /* {version=1, since=Dec 2013}*/

  public int id;
  public int destination;
  public int origin;
  public ArrayList  current;
  public ArrayList  out;

  /** 
   *  Creates an instance of a passanger with a specific destination
   */
  Passenger(int id, int origin, int destination) {
    this.id = id;
    this.destination = destination;
    this.origin = origin;
  }

  /** 
   *  Creates an instance of a passenger with a random destination. The destination id number is greater that the current station id
   */
  Passenger(int id) {
  }

  /** 
   *  gets passenger ID
   */
  public int getID() {
    return id;
  }

  /** 
   *  gets passenger Origin
   */
  public int getOrigin() {
    return origin;
  }

  /** 
   *  gets passenger detination
   */
  public int getDestination() {
    return destination;
  }

  /** 
   *  shows passenger on canvas. NOT implemented yet!!!
   */
  public void show() {
    //ellipse
  }
  
  // ******************************* Reports ************************
  
    /** 
   *  returns a report of the passenger: (id+" "+origin+" "+destination+"\n")
   */
  public String report(){
    String report = (id+" "+origin+" "+destination+"\n");
    return report;
  }
}