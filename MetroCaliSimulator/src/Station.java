import java.util.ArrayList;
import java.util.Comparator;
import java.util.TreeSet;

import processing.core.PApplet;
import processing.core.PVector;

/** 
 * Represents a station on a route. The passengers at current time 
 * populate the inPassenger collection. All the passengers that got off 
 * the station are stored in outPassenger collection.
 *
 * This class has the method operation() to execute and control its operation.
 * The operation is invoked from an external class inside a loop, usually associated to 
 * the draw() method.
 *
 * The operation() method has a discrete control of some aspects of the simulation time.
 * Each step in time is called a tick. The pace of ticks is associated to the system's clock.
 * Their duration is controlled with the variable tickModule. The larger the tickModule
 * the longer the tick.
 *
 * In this version the generation of a batch of new passengers in each station is synchonized 
 * to the tick pace. The atributes sistemControl, controlClock and tickModule define the 
 * discrete control of simulation time.
 */

public class Station {
  /* {author=Juan Salamanca, version=1, since=Dec 2013}*/
  private int id;
  private int count; // secuence of ids generated in this station
  private int totalNumberOfStops;
  private int demand;
  public PVector location;
  public int crowdSize;
  public int hasBus;
  public ArrayList <Passenger> inPassengers;
  public ArrayList <Passenger> outPassengers;
  public Dock myDock;
  public TreeSet <Bus> visitingBuses;
  private boolean tail; // true if the station is the head or the tail of the route. False by default.

  private int tickcount = 0;
  private int systemClock = 0; // stores the seconds of the computer's clock
  private int controlClock = 0; // allows only one operation per tick eventhough the method is invoked many times per second.  
  private int tickModule = 4; // defines the pace of ticks in seconds
  private float theTime = 0; // the millis() counter
  PApplet app;

  /** 
   * Creates a Station with an specific ID, demand and number of stops. 
   *
   * @param id the station id. It is an integer equal to or greater than 0
   * @param demand an indicator of the throughput of passengers at the station 
   * @param totalNumberOfStops a variable that allows the new passengers to decide the destination stop
   */
  Station(PApplet app, int id, int demand, int totalNumberOfStops) {
    this.app = app; 
	  this.id = id;
    this.demand = demand;
    this.totalNumberOfStops = totalNumberOfStops;
    tail = false;
    count = 0;
    myDock = new Dock(app);
    // coleecions of passengers
    inPassengers = new ArrayList();
    outPassengers = new ArrayList();

    // sets the collection of visiting buses sorted by their ID
    visitingBuses = new TreeSet <Bus>(new Comparator <Bus>() {
      public int compare(Bus a, Bus b) {
        return a.getID() - b.getID();
      }
    }
    );
  }

  /** 
   *  Creates a Station with an specific ID at an specific location
   *
   * @param id the station id. It is an integer equal to or greater than 0
   * @param demand an indicator of the throughput of passengers at the station 
   * @param location locates the station at a X,Y position on canvas 
   * @param totalNumberOfStops a variable that allows the new passengers to decide the destination stop
   */
  Station(int id, int demand, PVector location, int totalNumberOfStops) {
    this.id = id;
    this.demand = demand;
    this.totalNumberOfStops = totalNumberOfStops;
    tail = false;
    count = id;
    this.location = location;
    myDock = new Dock(app);
    visitingBuses = new TreeSet <Bus>(new Comparator <Bus>() {
      public int compare(Bus a, Bus b) {
        return a.getID() - b.getID();
      }
    }
    );
  }

  // ************************* methods for the station *************************
  /** 
   *  Plots the station on the field. This method includes the discrete control of some aspects of the simulation
   */
  public void operate(Bus bus) {
    if (isBusAtDock(bus)) {
      //For each passenger
      for (int i=0; i < inPassengers.size(); i++) {
        Passenger tempPass = inPassengers.get(i);
        //getBusStopList;
        for (int j=0; j < bus.getStopList().length ; j++) {
          //if passenger destination matches any of the bus stops
          if (tempPass.getDestination() == bus.getStopList()[j]) {
            // println("At station " + id + " passenger "+ tempPass.getID() +" can get on the bus "+ tempBus.getID());
            //if the bus has room
            if (bus.hasRoom()) {
              //passenger gets on the bus 
              bus.addPassenger(tempPass);
              //passenger is removed from inPassengers collection
              inPassengers.remove(tempPass);
              //println("At station " + id +" passengers at platform: "+ inPassengers.size() + " passengers on board: " + tempBus.getPassOnBoard());
            }
          }
        }
      }
    }

    theTime = app.floor((app.millis()/controlPanel.Panel.getControlP5().getValue("tickSpeed")));
    systemClock = app.round(theTime); // see anotation in attributes section at the begining of this class
    if (systemClock % tickModule == 0 && systemClock != controlClock) {
      //      print(report(true));
      tickcount ++;
      newPassengers();
      controlClock = systemClock;
    }
  }

  /**
   * Displays the station information on the canvas 
   */
  public void show() {
    myDock.show();
    if (tail) {
      app.fill(200, 10, 10);
      app.ellipse(location.x, location.y, 5, 5);
      app.text(id, location.x-9, location.y);
    } 
    else {
    	app.noStroke();
    	app.fill(74, 200, 50);
    	app.stroke(220, 220, 150, 50);
    	app.line(location.x, location.y - 10, location.x, location.y + 10);
      app.ellipse(location.x, location.y + 15, inPassengers.size(), inPassengers.size());
      app.fill(174, 200, 150);
      app.text(outPassengers.size(), location.x, location.y-15);
      //app.fill(174, 200, 50, 10);
      //app.ellipse(location.x, location.y - 15, outPassengers.size(), outPassengers.size());
      //app.rect(location.x, location.y - 15, 5, outPassengers.size()*-1);
      app.fill(174, 200, 50);
      app.text(id, location.x-9, location.y);
    }
  }

  /** 
   *  Gets the station ID
   */
  public int getID() {
    return id;
  }

  /** 
   *  Gets the station location
   */
  public PVector getLocation() {
    return location;
  }

  /** 
   *  sets the location of the station
   */
  public void setLocation(PVector location) {
    this.location = location;
    myDock.setLocation(location);
  }

  // ************************* methods for dock operation *************************

  /** 
   *  Returns the bus instance if at dock else null
   */
  private Bus getBusAtDock(Bus [] buses) {
    /* {author=Juan Salamanca, version=1, since=Dec 2013}*/
    Bus cBus = null;
    for (int i=0 ; i<buses.length; i++) {
      // if the bus stops at this stop
      if (buses[i].stopsAt(id)) {
        // if the bus is at the stop
        if (buses[i].getLocation().dist(location) < Bus_Route_Simulator_v1_4.getMinProximity()) { // compares with a global minProximity variable
          //println("bus "+buses[i].getID()+" is "+ buses[i].getLocation().dist(location) +" fom station "+ id);
          cBus = buses[i];
          saveBusAtDock(buses[i]);
          //println("bus "+busID+" at station "+ id);
        }
      }
    }
    return cBus;
  }

  /** 
   *  Returns the bus instance if at dock else null
   */
  private boolean isBusAtDock(Bus bus) {
    /* {author=Juan Salamanca, version=1, since=Jan 2014}*/
    boolean doesStop = false;
    // if the bus stops at this stop
    if (bus.stopsAt(id)) {
      // if the bus is at the stop
      if (bus.getLocation().dist(location) < Bus_Route_Simulator_v1_4.getMinProximity()) { // compares with a global minProximity variable
        //println("bus "+buses[i].getID()+" is "+ buses[i].getLocation().dist(location) +" fom station "+ id);
        doesStop = true;
        saveBusAtDock(bus);
        //println("bus "+busID+" at station "+ id);
      }
    }
    return doesStop;
  }

  /** 
   *  True if there is a bus at the station
   */
  public boolean getDockStatus(Bus [] buses) {
    /* {author=Juan Salamanca, version=1, since=Dec 2013}*/
    boolean dockFree = false;
    for (int i=0 ; i<buses.length; i++) {
      if (buses[i].getLocation().dist(location) < Bus_Route_Simulator_v1_4.getMinProximity()) { // compares with a global minProximity variable
        dockFree = true;
      }
    }
    return dockFree;
  }

  /** 
   *  Stores the visiting buses in a treeSet
   */
  private void saveBusAtDock(Bus bus) {
    /* {author=Juan Salamanca, version=1, since=Dec 2013}*/
    visitingBuses.add(bus);
  }

  // ************************* methods for passengers *************************

  /** 
   *  Generates a random number of passengers and adds them to the "inPassenger" collection
   */

  public void newPassengers() {
    /* {author=Juan Salamanca, version=1, since=Dec 2013}*/
    int batch = app.floor(app.random (0, demand));
    for (int i=0 ; i<batch; i++) {
      int destTemp = app.floor(app.random(id+1, totalNumberOfStops-1));
      if (destTemp > id) {
        Passenger pTemp = new Passenger((id*1000)+count, id, destTemp); //int id, int origin, int destination
        inPassengers.add(pTemp);
        count ++;
      }
    }
    //println("tick "+ tickcount+" station "+id+" has received "+count+" passengers today");
  }

  /** 
   *  returns the size of the "inPassenger" collection at that moment
   */
  public int getCrowdSize() {
    return inPassengers.size();
  }

  /** 
   *  returns the total number of passengers that got off at that station
   */
  public int getOffPassenger() {
    /* {author=Juan Salamanca, version=1, since=Dec 2013}*/
    return outPassengers.size();
  }

  /** 
   *  returns the total number of passengers that started their trip at this station
   */
  public int getTotalInPassengers() {
    return count;
  }

  /** 
   *  Sets the station at the tail or the head of its route
   */
  public void setTail() {
    this.tail = true;
  }

  /** 
   *  returns the total number of ticks
   */
  public int getTickcounter() {
    return tickcount;
  }

  // ******************************* Reports ************************

  /** 
   *  returns a description of the station: ("Station "+id+" "+demand+" "+location.x+" "+location.y+"\n");;
   */
  public String description() {
    String description = ("Station "+id+" "+demand+" "+location.x+" "+location.y+" "+tail+"\n");
    return description;
  }

  /** 
   *  returns a report of the station: ("Station "+id+" "+inPassengers+" "+outPassengers+" "+visitingBuses.size()+"\n");
   *  If the parameter is true the passenger report is printed
   */
  public String report(boolean pass) {
    String report = (id+" "+getCrowdSize()+" "+getOffPassenger()+" "+visitingBuses.size()+" "+tail+"\n");
    if (pass) {
      //report = report + "    Pass"+"\n";
      for (int i=0; i < inPassengers.size() ;i++) {
        Passenger temp = inPassengers.get(i);
        report = report+"    inPass "+temp.report();
      }
      for (int i=0; i < outPassengers.size() ;i++) {
        Passenger temp = outPassengers.get(i);
        report = report+"    outPass "+temp.report();
      }
    }
    return report;
  }
}