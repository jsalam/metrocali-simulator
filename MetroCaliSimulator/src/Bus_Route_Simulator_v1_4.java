import processing.core.PApplet;
import processing.core.PFont;
import processing.net.Server;
import controlP5.ControlP5;

//Libraries for the map
import processing.opengl.*;
import codeanticode.glgraphics.*;

//Other libraries
import java.util.*;

import de.fhpotsdam.unfolding.geo.Location;
import de.fhpotsdam.unfolding.utils.MapUtils;
import controlPanel.Panel;



/** 
 * @since  Jan 7 2014
 * @author Juan Salamanca
 */
public class Bus_Route_Simulator_v1_4 extends PApplet {

PFont defaultFont;
private Server server;
private Operation operation;
private static float minProximity = 2;
private Panel topPanel;
CityMap map;

public void setup() {
  size (1100, 500);
  // The core
  operation = new Operation(this, 1020); // operation duration in seconds

  // The server
  // server = new Server(this, 12345);

  // Style setup
  ellipseMode(CENTER);
  //rectMode(CENTER);
  defaultFont = loadFont("../data/SansSerif-10.vlw");
  textFont(defaultFont, 10);
  textAlign(CENTER, CENTER);
 
  // controlPanel
  topPanel = new Panel(this, width, 100);
  // The map
  map = new CityMap(this);

}

public static float getMinProximity(){
	return minProximity;
}

public void draw() {
  //background(100);
  map.draw();
  topPanel.show();
  operation.run();
  String outputRA = operation.printReport(0, true, false, false, true); //(int theRoute, stat, pass, buses, busesAtStation)
//  if (outputRA != null) {
//    server.write(outputRA);
//  }
}

  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "--full-screen", "--bgcolor=#666666", "--stop-color=#cccccc", "Bus_Route_Simulator_v1_4" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
