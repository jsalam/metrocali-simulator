import processing.core.PApplet;
import processing.core.PVector;


/** 
 *  Defines the area associated to a Station where buses stop
 */

public class Dock {

  private int dWidth;
  private int dHeight;
  private int totalBuses;
  private PVector location;
  public Station myStation;
  PApplet app;

  /** 
   *  Creates an instance of a Dock with specified values
   */
  public Dock(PApplet app, int dWidth, int dHeight) {
	  this.app = app;
	  app.println("nulo=? : " + app);
    this.dWidth = dWidth;
    this.dHeight = dHeight;
  }

  /** 
   *  Creates an instance of a Dock with default values
   */
  public Dock(PApplet app) {
	  this.app = app;
    dWidth = 10;
    dHeight = 5;
  }

  /** 
   *  dispalys the dock on screen
   */
  public void show() {
	  //app.println("nulo=? : " + app);
    app.fill(100);
    app.rect(location.x, location.y, dWidth, dHeight);
  }
  
    /** 
   *  sets the location of the dock
   */
  public void setLocation(PVector location) {
    this.location = location;
  }
  
  /** 
   *  True if there is a bus at the dock
   */
  public Boolean getDockStatus() {
    return null;
  }

  /** 
   *  Returns the number of buses that stoped at this dock
   */
  public int getTotalBuses() {
    return totalBuses;
  }
}