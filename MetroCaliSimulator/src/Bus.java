import java.util.ArrayList;

import processing.core.PApplet;
import processing.core.PVector;


public class Bus {

  private Route myRoute;
  private int id;
  private float speed;
  private int capacity;
  // the list of stops this bus is intended to stop at.
  private int [] stops; 
  // The size of this array is equal to the route station's size. Each position is true if the bus stops at that station
  private boolean [] stopsBoolean; 
  private ArrayList <Passenger> onBoard;
  private int stopSequence;
  //private int busAtStation;
  private PVector location;
  private PVector destination;
  private boolean inMotion;
  public boolean outOfService;
  private boolean atStation;
  // stats
  private int passCounter; // counter of passengers 
  private int gotIn; // passengers that got onBoard at one station 
  private int gotOff; // passengers that got offBoard at one station
  public String reportAtStop;
  PApplet app;

  /** 
   *  Creates an instance of a bus class that stops in all the route stations and has default capacity 
   */
  public Bus(PApplet app, int id) {
	this.app = app;
    this.id = id;
    speed = 0.025f;
    capacity = 60;
    onBoard = new ArrayList();
    stopSequence = 0;
    outOfService = false;
    inMotion = false;
    atStation = false;
    passCounter = 0;
    reportAtStop = id+" Not at station";
  }

  /** 
   * Initiates the bus on the route by setting its stop list (boolean), location and next destination. Destination is different than stop, 
   * it refers to the location of the next station
   * @param theRoute refers to the route this bus is running on
   */
  public void setupRoute (Route theRoute) {
    myRoute = theRoute;
    stops = myRoute.getStationsID();
    stopsBoolean = new boolean [stops.length];
    for (int i=0; i<stopsBoolean.length ; i++) {
      stopsBoolean[i] = true;
    }
    PVector temp = getNextStopLocation();
    location = new PVector(temp.x, temp.y) ;
    destination = location;
  }

  /** 
   * Initiates the bus on the route and sets its route, location and next destination. Destination is different than a stop, 
   + it refers to the location of the next station
   */
  public void setupStops (int [] stops) {
    this.stops = stops;
    stopSequence = 0;
    // reset the stopBoolean to false
    for (int i=0; i<stopsBoolean.length ; i++) {
      stopsBoolean[i] = false;
    }
    // set true to the positions in the stop list
    try {
      for (int j=0; j<stops.length ; j++) {
        stopsBoolean[stops[j]] = true;
      }
    }
    catch (Exception e) {
    	app.println ("WARNING!! It is likely that the bus "+ id +" on the route "+ myRoute.getID()+" is intended to stop in non existant stations. Check the stop list assigned to this bus.");
    	app.println ("As an example, if your route has N stations, then the bus' stop list must start with stop 0 and finish with stop N-1. The stations inbetween must be less than N-1 and could be set in any order.");
    	app.println(e);
    }

    PVector temp = getNextStopLocation();
    location = new PVector(temp.x, temp.y) ;
    destination = location;
  }

  /** 
   *  Shows the bus on the field
   */
  public void run(float tickSpeed) {
    // adjusts bus speed to the slider input
    speed = app.map(tickSpeed, 1000, 100, 0.01f, 0.05f);

    // When the bus arrives to the station gets the next destination. This happens only once
    if (!inMotion && stopSequence < stopsBoolean.length) {
      destination = getNextStopLocation();
      reportAtStop = getReportAtStop();
      gotIn = 0;
      gotOff = 0;
      inMotion = true;
    }

    if (inMotion && !atStation) {
      moveBus();
    }


    Station tempStation = getCurrentStation();
    if (tempStation != null) {
      //For each passenger
      for (int i=0; i < onBoard.size(); i++) {
        Passenger tempPass = onBoard.get(i);
        //if passenger destination matches station ID
        if (tempPass.getDestination() == tempStation.getID()) {
          //println("Bus at " + tempStation.getID()+ " station. This is the stop of passenger "+ tempPass);
          //passenger gets on the Station 
          tempStation.outPassengers.add(tempPass);
          //passenger is removed from onBoard collection
          removePassenger(tempPass);
          //println("At station " + id +" passengers at platform: "+ inPassengers.size() + " passengers on board: " + tempBus.getPassOnBoard());
        }
      }
    }

    if (outOfService) { 

      if (tempStation != null && !(getCurrentStation().getID() == stopsBoolean.length-1)) {
    	  app.fill (255, 0, 0);
    	  app.text(id+ "_OoS", location.x - 4, location.y +34);
    	  app.rect(location.x, location.y + 6, onBoard.size(), 5);
    	  app.noFill();
    	  app.rect(location.x, location.y + 6, capacity, 5);
        //printOnBoard();
      }
    }
    else {
    	app.fill(200);
    	app.text(id, location.x - 4, location.y +34);
    	app.text("onBoard: "+onBoard.size(), location.x - 4, location.y +44);
    	app.rect(location.x, location.y + 6, onBoard.size(), 5);
    	app.noFill();
    	app.rect(location.x, location.y + 6, capacity, 5);
    }
  }


  /** 
   *  gets the location of the next station of its stop list
   */
  public PVector getNextStopLocation() {
    getServiceStatus();
    PVector stationLocation = location;
    // if this bus is scehduled to stop in the next stop
    if (stopsBoolean[stopSequence]) {
      // gets the next station location
      stationLocation = myRoute.getStationLocation(stopSequence);
      //println("Bus "+id+" Heading to Station: "+stopSequence);
    }
    stopSequence ++;
    return stationLocation;
  }

  /** 
   *  True if out of service. Evaluates if any of the forthcoming stations is scheduled in its route  
   */
  private void getServiceStatus() {
    int cont = stopSequence;
    if (stopSequence == stopsBoolean.length-1) { 
      outOfService = true;
    }
    else {
      while ( cont < stopsBoolean.length-1 && stopsBoolean[cont] == false) {
        outOfService = true;
        cont++;
      }
      if (cont < stopsBoolean.length-1) {
        outOfService = false;
      }
    }
  }

  /** 
   *  animation to move the bus to the next station in route
   */

  public void moveBus() {
    // println("Dist of Bus "+id+" to next stop: "+location.dist(destination));
    if (location.dist(destination) > Bus_Route_Simulator_v1_4.getMinProximity()) {
      location.lerp(destination.x, destination.y, 0.0f, speed);
    }
    else {
      inMotion = false; // the bus arived to the destination
      atStation = false;
    }
  }

  /** 
   *  Returns the current location of the bus
   */
  public PVector getLocation() {
    return location;
  }

  /** 
   *  Returns the bus id
   */
  public int getID() {
    return id;
  }

  /** 
   *  Returns the bus' stop list
   */
  public int [] getStopList() {
    return stops;
  }

  /** 
   *  true if the bus has room for more passengers
   */
  public boolean hasRoom() {
    if (onBoard.size() < capacity) {
      return true;
    }
    else {
      return false;
    }
  }

  /** 
   *  adds passenger onBoard
   */
  public void addPassenger(Passenger pass) {
    onBoard.add(pass);
    passCounter ++;
    gotIn++;
  }

  /** 
   *  removes passenger onBoard
   */
  public void removePassenger(Passenger pass) {
    onBoard.remove(pass);
    gotOff++;
  }

  /** 
   *  Returns the station instance if the bus is at its dock, else null
   */
  private Station getCurrentStation() {
    Station cStation = null;
    for (int i=0 ; i<myRoute.getStations().length; i++) {
      if (myRoute.getStation(i).getLocation().dist(location) < Bus_Route_Simulator_v1_4.getMinProximity()) { // compares with a global minProximity variable
        // println("bus "+id+" is at Station "+ myRoute.getStation(i).getID());
        cStation = myRoute.getStation(i);
      }
    }
    return cStation;
  }

  /** 
   *  Prints the list of passengers onBoard
   */
  public void printOnBoard() {
    for (int i=0 ; i<onBoard.size(); i++) {
      Passenger temp = onBoard.get(i);
     app.println("Psngr "+temp.getID()+" heading to "+temp.getDestination());
    }
  }

  /** 
   *  true if the bus stops at a station id
   *  @param theStopID it is the ID number of the stop on the route
   */
  public boolean stopsAt(int theStopID) {
    boolean temp = false;
    for (int i=0 ; i<stops.length; i++) {
      if (stops[i] == theStopID) {
        temp = true;
      }
    }
    return temp;
  }

  // ******************************* Reports ************************

  /** 
   *  returns a description of the bus: (id speed capacity);
   */
  public String description() {
    String description = ("Bus "+id+" "+speed+" "+capacity+"\n");
    return description;
  }

  /** 
   *  returns on-demand report of the bus: (id onBoardSize passCounter location.x location.y)
   */
  public String reportOnDemand() {
    int onBoardSize = 0;
    if (onBoard == null) {
      onBoardSize = 0;
    }
    else {
      onBoardSize = onBoard.size();
    }
    String report = (id+" "+onBoardSize+" "+passCounter+" "+location.x+" "+location.y+"\n");
    return report;
  }

  /** 
   *  returns on-stop report of the bus: (id remainingCapacity gotIn gotOut station);
   */
  public String getReportAtStop() {
    int onBoardSize = 0;
    if (onBoard == null) {
      onBoardSize = 0;
    }
    else {
      onBoardSize = onBoard.size();
    }
    String report = (id+" "+(capacity - onBoardSize)+" "+gotIn+" "+gotOff+" "+getCurrentStation().getID()+"\n");
    return report;
  }
}