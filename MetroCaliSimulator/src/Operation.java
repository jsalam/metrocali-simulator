/** 
 * Represents the operation of buses on routes. 
 *
 * This class has the method run() to execute and control its own operation.
 * The method run() is invoked from an external class inside a loop, usually associated to 
 * the draw() method.
 *
 * The run() method has a discrete control of some aspects of the simulation time.
 * Each step in time is called a tick. The pace of ticks is associated to the system's clock.
 * Their duration is controlled with the variable tickModule. The larger the tickModule
 * the longer the tick.
 *
 * In this version the generation of a batch of new passengers in each station is synchronized 
 * to the tick pace. The attributes sistemControl, controlClock and tickModule define the 
 * discrete control of simulation time.
 */

import processing.core.PApplet;
import processing.core.PVector;

public class Operation {
	PApplet app;

  //Operation duration
  private int totalMinutes;

  // Tick control
  private int systemClock = 0; // stores the seconds of the computer's clock
  private float theTime = 0;
  private int tickModule = 4; // defines the pace of ticks in seconds
  private int controlClock = 0; // allows only one operation per tick eventhough the method is invoked many times per second.  


  //Routes
  Route routeA;
  Route routeB;
  Route routeC;

 public Operation(PApplet app, int totalMinutes) {
	  this.app = app;

    this.totalMinutes = totalMinutes;

    // Routes setup
    routeA = new Route(app, "T31", 27, 13, 150); // (name / stations / tickmodule / verticalPosition)
    routeB = new Route(app, "T50", 13, 17, 250);
    routeC = new Route(app, "T47A", 10, 17, 350); 

    // assigns a station on route B to route A 
    Station temp = routeB.getStation(5);
    PVector vTemp = temp.getLocation();
    //vTemp.y = 250;
    routeA.setStation(5, temp);
    routeC.setStation(5, temp);

    // Customized stop list
    int [] stops100 = {
      0, 1, 7, 9, 15, 16, 25
    }; 
    int [] stops102 = {
      0, 1, 3, 5, 6, 4, 8, 15
    }; 

    app.println("********* Run client to see the operation report ************ \n");
  }

  /** 
   *  Executes the operation. Starts the master Clock, the fleet operation and the routes. 
   */
  public void run() {

    // Operation control
    if (theTime < totalMinutes) {
      // Time control
  
      theTime = app.floor((app.millis()/controlPanel.Panel.getControlP5().getValue("tickSpeed")));
      systemClock = app.round(theTime); // see anotation in attributes section at the begining of this class

      // Routes operation
      routeA.runRoute(systemClock);
      routeB.runRoute(systemClock);
      routeC.runRoute(systemClock);
    }

    // Messages on canvas
    app.textAlign(app.LEFT, app.CENTER);
    app.fill(220, 220, 150, 90);
    app.text("Operation elapsed time: "+ theTime + " minutes, out of "+totalMinutes, 20, 30);
    app.textAlign(app.CENTER, app.CENTER);

    // timeBar
    app.rectMode(app.LEFT);
    app.noFill();
    app.rect(20, 20, app.width-20, 13);
    if (theTime < totalMinutes) {
    	app.fill(220, 220, 150, 90);
    }
    else { 
    	app.fill(220, 0, 0, 50);
    }
    app.rect(20, 20, app.map(theTime, 0, totalMinutes, 20, app.width-20), 13);
    app.rectMode(app.CENTER);
  }
  // ******************************* Reports ************************

  /** 
   *  returns a report of the opertation time: (int theRoute, boolean stat, boolean buses, boolean pass)
   */
  public String printReport(int theRoute, boolean stat, boolean pass, boolean buses, boolean busesAtStation) {
    String data = null;
   // if (theTime >= totalMinutes) data = "END OF OPERATION\n";
    //Report generation
    if (systemClock % tickModule == 0 && systemClock != controlClock) {
      switch(theRoute) {
      case 0:
        data = report(routeA, stat, pass, buses, busesAtStation);
        break;
      case 1:
        data = report(routeB, stat, pass, buses, busesAtStation);
        break;
      case 2:
        data = report(routeC, stat, pass, buses, busesAtStation);
        break;
      }
      controlClock = systemClock;
    }

    return data;
  }

  /** 
   *  returns a report of the opertation time: ("Operation time "+systemClock+"\n");
   */
  private String report(Route R, boolean stat, boolean pass, boolean buses, boolean busesAtStation) {
    String report = ("Operation_time "+systemClock+"\n");
    report = report+" "+R.report(stat, pass, buses, busesAtStation);
    return report;
  }
}