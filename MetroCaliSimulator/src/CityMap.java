import de.fhpotsdam.unfolding.*;
import de.fhpotsdam.unfolding.geo.*;
import de.fhpotsdam.unfolding.utils.*;
import de.fhpotsdam.unfolding.providers.*;
import processing.core.*;

public class CityMap extends UnfoldingMap {

	PApplet app;

	public CityMap(PApplet app) {
		super(app, new OpenStreetMap.OpenStreetMapProvider());
		this.app = app;
		this.zoomAndPanTo(new Location(3.3672, -76.52), 15);
		MapUtils.createDefaultEventDispatcher(app, this);
	}

}
